//Our Services

let tab = function () {
	let tabNav = document.querySelectorAll('.tabs-nav_item'),
		tabContent = document.querySelectorAll('.tab'),
		tabName;

	tabNav.forEach(item => {
		item.addEventListener('click', selectTabNav)
	});

	function selectTabNav() {
		tabNav.forEach(item => {
			item.classList.remove('is-active')
		});
		this.classList.add('is-active');
		tabName = this.getAttribute('data-tab-name');
		selectTabContent(tabName);
	};

	function selectTabContent(tabName) {
		tabContent.forEach(item => {
			item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
			console.log(tabName);
		})
	}
};
tab();


// Slider

$(document).ready(function () {
	$('.slider').slick({
		arrows: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		speed: 1000,
		autoplaySpeed: 800,
		asNavFor: (".sliderbig"),
		// centerMode: true,
		focusOnSelect: true,
		infinite: true,
		swipeToSlide: true,
		easing: 'ease',
		// wariableWidth: true,
		// centerPadding: '100px',
		lazyLoad: 'ondemand',
	});

	$('.sliderbig').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: (".slider"),
	});
});


// Amazing

function AmazingWorks() {
	const filters = [...document.querySelectorAll(`.filter__item`)];
	const filtersContent = [];
	const filtersContentList = document.querySelector(`.filter__content-list`);
	const countOfWorks = 36;
	const btnLoadMore = document.querySelector(`.amazing-works .btn-load-more`);

	for (let i = 1; i <= countOfWorks; i++) {
		const li = document.createElement(`li`);
		li.classList.add(`filter__content-item`);

		li.setAttribute(`data-filter`, rundomDataAtribute(i));

		li.innerHTML = `
				<img src="./img/amazing-works/amazing/graphic-design${i}.jpg" alt="amazing-works-${i}" width="284" height="206"
				class="filter__content-img">
				<div class="filter__content-description-wrapper">
					<div class="filter__content-description-links">
						<a href="#" class="filter__content-description-link">...</a>
						<a href="#" class="filter__content-description-link">...</a>
					</div>
                  
					<p class="filter__content-description-title">
						creative design
					</p>
					<p class="filter__content-description-text">
                    Graphic Design
					</p>
				</div>
			`;

		filtersContent.push(li);
	}

	for (let i = 1; i <= 12; i++) {
		const li = filtersContent.splice(Math.floor(Math.random() * filtersContent.length), 1);
		filtersContentList.append(li[0]);
	}

	filters.forEach(btn => {
		btn.addEventListener(`click`, filteringOnClick);
	});
	btnLoadMore.addEventListener(`click`, btnLoadMoreOnClick);

	function rundomDataAtribute(i) {
		if (!(i % 4)) return `graphic-design`;
		if (!(i % 3)) return `web-design`;
		if (!(i % 2)) return `landing-pages`;
		return `wordpress`;
	}

	function btnLoadMoreOnClick() {
		btnLoadMore.classList.add(`btn-load-more__loading`);
		setTimeout(() => {
			filters.forEach(btn => {
				if (btn.getAttribute(`data-filter`) === `all`) {
					btn.classList.add(`filter__item--active`);
				} else {
					btn.classList.remove(`filter__item--active`);
				}
			});

			document.querySelectorAll(`.filter__content-item`).forEach(element => {
				element.style.display = `block`;
			});

			btnLoadMore.classList.remove(`btn-load-more__loading`);
			for (let i = 1; i <= 12; i++) {
				if (filtersContent.length === 0) break;
				const li = filtersContent.splice(Math.floor(Math.random() * filtersContent.length), 1);
				filtersContentList.append(li[0]);
			}
			if (filtersContent.length === 0) {
				btnLoadMore.style.display = `none`;
			}
		}, 2000);
	}

	function filteringOnClick(e) {
		filters.forEach(btn => {
			if (btn === e.target) {
				e.target.classList.add(`filter__item--active`);
			} else btn.classList.remove(`filter__item--active`);
		});

		const filtersContent = [...document.querySelectorAll(`.filter__content-item`)];
		if (e.target.getAttribute(`data-filter`) === `all`) {
			filtersContent.forEach(element => {
				element.style.display = `block`;
			});
		}
		else {
			filtersContent.forEach(element => {
				if (e.target.getAttribute(`data-filter`) === element.getAttribute(`data-filter`)) {
					element.style.display = `block`;
				}
				else element.style.display = `none`;
			});
		}
	}

}
AmazingWorks();